requires

- have ssh key generated with `ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/id_ed25519`
- have that key whitelisted in gitlab and `mykeys` 
- `git` to checkout this repository
- `make` and `bash` to run this command
