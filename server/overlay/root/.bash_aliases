
alias youtube-dl='youtube-dl --prefer-ffmpeg'
alias youtube-dl='youtube-dl --prefer-ffmpeg'
alias mybuntu='docker run --rm -it -v $PWD:/w --workdir /w mybuntu:xenial bash'
#alias mynord='docker run --rm -it -v $PWD:/w --privileged mynord:latest bash'
alias mynordde='docker run --rm -it -v $PWD:/w --privileged -e SERVER=de677 mynord:latest bash'
alias dff='df -h /'

function mynord() {
	docker run --rm -it -v $PWD:/w --privileged -e COUNTRY=$1 mynord:latest bash;
}
