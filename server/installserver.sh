#!/usr/bin/env bash

apt-get update && apt-get upgrade -y
DEBIAN_FRONTEND=noninteractive apt-get install -y curl dstat htop wget pv apache2 iputils-ping git vim ffmpeg ruby-dev silversearcher-ag build-essential ncdu uuid-runtime netcat jq unrar aria2 locales fail2ban docker.io tmux
DEBIAN_FRONTEND=noninteractive apt-get install -y bat
# DEBIAN_FRONTEND=noninteractive apt-get install -y python3-certbot-apache certbot

# youtube-dl current version
curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
chmod a+rx /usr/local/bin/youtube-dl

#curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
apt-get install -y nodejs
# npm install -g bower

apt-get install -y openjdk-8-jdk ant maven php php-curl python python3

# sdk man
curl -s "https://get.sdkman.io" | bash


# create and apply overlay
tar -C server/overlay/ -cvf overlay.tar .
# tar -xvkf overlay.tar -C /  # do not overwrite
tar -xvf overlay.tar -C /
rm overlay.tar


# checkout repositories
git clone https://gitlab.com/OutOfBrain/mykeys.git ~/.ssh/mykeys
git clone https://gitlab.com/OutOfBrain/mybuntu.git ~/dev/dockerstuff/mybuntu


# setup ssh keys
ln -s /root/.ssh/mykeys/authorized_keys /root/.ssh/authorized_keys


# setup certbot
# certbot --no-redirect
